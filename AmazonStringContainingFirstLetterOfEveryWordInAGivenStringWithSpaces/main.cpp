#include <iostream>

using namespace std;

class Engine
{
    private:
        string str;
    
    public:
        Engine(string s)
        {
            str = s;
        }
    
        void displayFirstCharacterInAString()
        {
            int  len  = (int)str.length();
            bool flag = false;
            for(int i = 0 ; i < len ; i++)
            {
                if(str[i] == ' ')
                {
                    flag = false;
                }
                else if(str[i] != ' ' && !flag)
                {
                    flag = true;
                    cout<<str[i]<<" ";
                }
            }
            cout<<endl;
        }
};

int main(int argc, const char * argv[])
{
    string str = "happy   coding";
    Engine e   = Engine(str);
    e.displayFirstCharacterInAString();
    return 0;
}
